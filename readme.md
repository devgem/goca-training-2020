# GOCA Kubernetes training 2020

## Day 1

### Slides  (will become available after day 1)

* [Pdf](https://gitlab.com/devgem/goca-training-2020/blob/master/slides/goca-kubernetes.pdf)
* [Powerpoint](https://gitlab.com/devgem/goca-training-2020/blob/master/slides/goca-kubernetes.pptx)

### Demos and exercises (will become available as training progresses)

#### Docker

1. [Intro](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/01%20docker%20intro.md)
2. [Create image](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/02%20docker%20create%20image.md)
3. [Linking containers](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/03%20docker%20linking%20containers.md)
4. [Volumes](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/04%20docker%20volumes.md)
5. [Multiple docker containers](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/05%20multiple%20docker%20containers.md) [[solution](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/05a%20multiple%20docker%20containers.md)] [[solution in script](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/5a-solution.sh)]
6. [Reverse proxy on hostheader](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/06%20reverse%20proxy%20on%20hostheader.md)

### Kubernetes

7. [Dashboard](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/07%20kubernetes%20dashboard.md)
8. [Pod](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/08%20kubernetes%20pod.md)
9. [Service](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/09%20kubernetes%20service.md)
10. [Ingress](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/10%20kubernetes%20ingress.md)
11. [Multi API resources](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/11%20kubernetes%20multi%20api%20resource.md) [[solution](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/11a%20kubernetes%20multi%20api%20resource.md)]
12. [Namespace](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/12%20kubernetes%20namespace.md)
13. [SSL](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/13%20kubernetes%20ssl.md)
14. [Volumes](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/14%20kubernetes%20volumes.md)
15. [Deployment](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/15%20kubernetes%20deployment.md) [[solution](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/15a%20kubernetes%20deployment.md)]
16. [Scale and loadbalance](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/16%20kubernetes%20scale%20and%20loadbalance.md)
17. [Rolling updates](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/17%20kubernetes%20rolling%20updates.md)
18. [Probing](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/18%20kubernetes%20probe.md) [[solution](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/18a%20kubernetes%20probe.md)]
19. [Demo in staging](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/19%20kubernetes%20demo%20in%20staging.md) [[solution](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/19a%20kubernetes%20demo%20in%20staging.md)]
20. [Helm](https://gitlab.com/devgem/goca-training-2020/blob/master/demo/20%20kubernetes%20helm.md)

## Day 2

### Helm

21. [Custom Helm chart](https://gitlab.com/devgem/goca-training-2020/-/blob/master/demo/21%20custom%20helm%20chart.md) [[solution](https://gitlab.com/devgem/goca-training-2020/-/blob/master/demo/21a%20custom%20helm%20chart.md)]

### Kubespray

22. [Kubespray](https://gitlab.com/devgem/goca-training-2020/-/blob/master/demo/22%20kubespray.md) [[solution](https://gitlab.com/devgem/goca-training-2020/-/blob/master/demo/22a%20kubespray.md)]

### Istio

23. [Istio](https://gitlab.com/devgem/goca-training-2020/-/blob/master/demo/23%20istio.md)
