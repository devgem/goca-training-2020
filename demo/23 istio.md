# Istio

Reset you Kubernetes cluster.

```shell
# get istio 1.7.3 from <https://github.com/istio/istio/releases> and unzip it

cd istio-1.7.3
cd bin

# install istio
./istioctl.exe install --set profile=demo

# enable sidecar injection in default namespace
kubectl label namespace default istio-injection=enabled

# check configuration
./istioctl.exe analyze

# install sample application
cd ..
kubectl apply -f samples/bookinfo/platform/kube/bookinfo.yaml
# check services
kubectl get services
# check pods
kubectl get pods

# check with Lens if everything is running in the default namespace

# double check
kubectl exec "$(kubectl get pod -l app=ratings -o jsonpath='{.items[0].metadata.name}')" -c ratings -- curl -s productpage:9080/productpage | grep -o "<title>.*</title>"
# should return <title>Simple Bookstore App</title>

# create ingress
kubectl apply -f samples/bookinfo/networking/bookinfo-gateway.yaml

# check configuration
cd bin
./istioctl.exe analyze
cd ..

# get ingress port
kubectl get svc istio-ingressgateway -n istio-system
# if we are lucky port 80 is open and we can see the sample application on <http://localhost/productpage>

# deploy the Kiali dashboard, along with Prometheus, Grafana, and Jaeger
kubectl apply -f samples/addons
while ! kubectl wait --for=condition=available --timeout=600s deployment/kiali -n istio-system; do sleep 1; done

# open  Kiali dashboard
cd bin
./istioctl.exe dashboard kiali

# check out the graph

# request routing

# apply default destination rules
kubectl apply -f samples/bookinfo/networking/destination-rule-all.yaml

# virtual service for all going to v1
kubectl apply -f samples/bookinfo/networking/virtual-service-all-v1.yaml

# check the destination rules
kubectl get destinationrules -o yaml

# route all traffic for user jason to v2
kubectl apply -f samples/bookinfo/networking/virtual-service-reviews-test-v2.yaml

# check out metrics
cd bin
./istioctl.exe dashboard grafana
# check <http://localhost:3000/dashboard/db/istio-mesh-dashboard>

# check out tracing
./istioctl.exe dashboard jaeger
```

## Exercise for the brave (homework)

Create an Istio configuration for the `demo` application:

* with dual versions (v1.1 and v1.2) for the backend
* using a gateway i.o. ingress
* with a rule to send 60% of the traffic to v1.1 and 40% to v1.2
* with tcp-metrics for the mongodb database
