# 03 Docker linking containers

## Demo

* run a sql server 2019 container, see <https://hub.docker.com/_/microsoft-mssql-server>

```shell
docker run --name sqlserver2019 -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=goca' -e 'MSSQL_PID=Express' -d mcr.microsoft.com/mssql/server:2019-latest
```

* link the sqlpad container to the sql server

```shell
docker run --name sqlpad --link sqlserver2019:sqlserver -d -p 3000:3000 sqlpad/sqlpad
```

* check out the containers

```shell
docker container list -a
```

* check why sql server did not start

```shell
docker logs sqlserver2019
```

* remove the the containers and recreate them properly

```shell
docker container rm $(docker container list -aq)
docker run --name sqlserver2019 -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Goca2019!' -e 'MSSQL_PID=Express' -d mcr.microsoft.com/mssql/server:2019-latest
docker run --name sqlpad --link sqlserver2019:sqlserver -d -p 3000:3000 sqlpad/sqlpad
```

* check out the containers

```shell
docker container list -a
docker container inspect <CONTAINERID_OR_CONTAINERNAME>
```

* check out the sqlpad site on <http://localhost:3000>
* create a connection to the sqlserver container
* do a query

```sql
SELECT * FROM master.sys.databases;
```

* create a database

```sql
CREATE DATABASE goca;
```

* check databases again

```sql
SELECT * FROM master.sys.databases;
```

* create a table with some data

```sql
USE goca;
CREATE TABLE Persons (
    PersonID int,
    LastName varchar(255),
    FirstName varchar(255)
);
INSERT INTO Persons (PersonID, LastName, FirstName)
VALUES
    (1, 'Bill', 'Gates'),
    (2, 'Steve', 'Jobs'),
    (3, 'Elon', 'Musk'),
    (4, 'Jeff', 'Bezos'),
    (5, 'Gavin', 'Belson');
```

* query the table

```sql
USE goca;
SELECT * FROM Persons;
```

* stop and remove the sql server 2019 container

```shell
docker container rm --force sqlserver2019
```

* recreate the sql server 2019 container

```shell
docker run --name sqlserver2019 -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Goca2019!' -e 'MSSQL_PID=Express' -d mcr.microsoft.com/mssql/server:2019-latest
```

* do a query

```sql
SELECT * FROM master.sys.databases;
```
