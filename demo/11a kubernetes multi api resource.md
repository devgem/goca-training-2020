# 11a Kubernetes multi api resource

## Solution

* create a yaml file with the your-app in `kube/your-app.yaml`

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: your-app-pod
  labels:
    app: your-app-pod
spec:
  containers:
    - name: your-app-container
      image: devgem/my-app:3.0
      env:
        - name: my_message
          value: "Hello from yourapp on Kubernetes"
      resources:
        limits:
          cpu: 1500m
          memory: 1024Mi
        requests:
          cpu: 50m
          memory: 50Mi
---
apiVersion: v1
kind: Service
metadata:
  name: your-app-service
spec:
  selector:
    app: your-app-pod
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: your-app-ingress
spec:
  rules:
    - host: yourapp.goca-training.be
      http:
        paths:
          - backend:
              serviceName: your-app-service
              servicePort: 80
```

* open `C:\Windows\System32\drivers\etc\hosts` in Notepad running as Administator
* add lines

```shell
	127.0.0.1       yourapp.goca-training.be
```

* deploy yourapp

```shell
kubectl apply -f kube/your-app.yaml
```

* goto the exposed app: <http://yourapp.goca-training.be>