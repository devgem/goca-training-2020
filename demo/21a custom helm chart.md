# 21a Custom Helm chart

## solution

* `demo-db.yaml`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: {{ .Release.Namespace }}
  name: demo-db-mongodb
  labels:
    app: demo-db-mongodb
spec:
  replicas: 1
  selector:
    matchLabels:
      app: demo-db-mongodb
  template:
    metadata:
      namespace: staging
      labels:
        app: demo-db-mongodb
    spec:
      containers:
        - name: demo-db-mongodb
          image: mongo:latest
---
apiVersion: v1
kind: Service
metadata:
  namespace: {{ .Release.Namespace }}
  labels:
    application: demo-db-mongodb
  name: demo-db-mongodb
spec:
  selector:
    app: demo-db-mongodb
  ports:
    - port: 27017
      protocol: TCP
      targetPort: 27017
```

* `demo-backend.yaml`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: {{ .Release.Namespace }}
  name: demo-backend
  labels:
    app: demo-backend
spec:
  replicas: 2
  selector:
    matchLabels:
      app: demo-backend
  template:
    metadata:
      namespace: {{ .Release.Namespace }}
      labels:
        app: demo-backend
    spec:
      containers:
        - name: demo-backend
          image: jeankedotcom/demo-backend:{{ .Values.service.version.backend }}
          resources:
            limits:
              cpu: 1500m
              memory: 1024Mi
            requests:
              cpu: 50m
              memory: 50Mi
---
apiVersion: v1
kind: Service
metadata:
  namespace: {{ .Release.Namespace }}
  labels:
    application: demo-backend
  name: demo-backend
spec:
  selector:
    app: demo-backend
  ports:
    - port: 80
      protocol: TCP
      targetPort: 80
  {{- if not .Values.ingress.enabled }}
  type: NodePort
  {{- end }}
{{- if .Values.ingress.enabled }}
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  namespace: {{ .Release.Namespace }}
  name: demo-backend
spec:
  rules:
    - host: {{ .Values.service.url.backend }}
      http:
        paths:
          - backend:
              serviceName: demo-backend
              servicePort: 80
{{- end }}
```

* `demo-frontend.yaml`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: {{ .Release.Namespace }}
  name: demo-frontend
  labels:
    app: demo-frontend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: demo-frontend
  template:
    metadata:
      namespace: {{ .Release.Namespace }}
      labels:
        app: demo-frontend
    spec:
      containers:
        - name: demo-frontend
          image: jeankedotcom/demo-frontend:v2.1
          env:
            - name: REST_API_URL
              value: "http://{{ .Values.service.url.backend }}"
          resources:
            limits:
              cpu: 1500m
              memory: 1024Mi
            requests:
              cpu: 50m
              memory: 50Mi
---
apiVersion: v1
kind: Service
metadata:
  namespace: {{ .Release.Namespace }}
  labels:
    application: demo-frontend
  name: demo-frontend
spec:
  selector:
    app: demo-frontend
  ports:
    - port: 80
      protocol: TCP
      targetPort: 80
  {{- if not .Values.ingress.enabled }}
  type: NodePort
  {{- end }}
{{- if .Values.ingress.enabled }}
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  namespace: {{ .Release.Namespace }}
  name: demo-frontend
spec:
  rules:
    - host: {{ .Values.service.url.frontend }}
      http:
        paths:
          - backend:
              serviceName: demo-frontend
              servicePort: 80
{{- end }}
```

* `values.yaml`

```yaml
service:
  url:
    frontend: "demo.goca-training.be"
    backend: "demo-api.goca-training.be"
  version:
    backend: "v1.1"
ingress:
  enabled: true
```

* `Chart.yaml`

```yaml
apiVersion: v2
name: demo
description: Demo application

# A chart can be either an 'application' or a 'library' chart.
#
# Application charts are a collection of templates that can be packaged into versioned archives
# to be deployed.
#
# Library charts provide useful utilities or functions for the chart developer. They're included as
# a dependency of application charts to inject those utilities and functions into the rendering
# pipeline. Library charts do not define any templates and therefore cannot be deployed.
type: application

# This is the chart version. This version number should be incremented each time you make changes
# to the chart and its templates, including the app version.
# Versions are expected to follow Semantic Versioning (https://semver.org/)
version: 1.0.0

# This is the version number of the application being deployed. This version number should be
# incremented each time you make changes to the application. Versions are not expected to
# follow Semantic Versioning. They should reflect the version the application is using.
appVersion: 1.0.0
```

* `NOTES.txt`

```text
Application demo installing...
```
