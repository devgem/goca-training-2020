# 22 Kubespray

Follow the instructions on <https://gitlab.com/devgem/kubespray-console>

You can find the key to access the servers on <https://gitlab.com/devgem/goca-training-2020/tree/master/key>

(you can delete the calicorr stuff in the `inventory.ini` file)
(make sure to use `kube_version: 1.18.9` we will upgrade to `1.19.2` later)

Once the cluster is installed check it with:

```shell
kubectl --kubeconfig=admin.conf get nodes
```

Once the cluster is installed we will upgrade it to `v1.19.2` over noon

Please use the following IP-addresses:

## Jan

| node | address | master | worker | etcd |
|-----|-----|-----|-----|-----|
| node 1 | 188.166.95.218 | • | • | • |
| node 2 | 188.166.95.224 | • | • | • |
| node 3 | 167.71.79.181 | | • | • |
| node 4 | 167.71.68.0 | | • | |
| node 5 | 188.166.94.44 | | • | |

## Geert

| node | address | master | worker | etcd |
|-----|-----|-----|-----|-----|
| node 1 | 188.166.97.43 | • | • | • |
| node 2 | 134.209.206.123 | • | • | • |
| node 3 | 157.245.64.171 | | • | • |
| node 4 | 188.166.96.129 | | • | |
| node 5 | 157.245.78.158 | | • | |

## Guy

| node | address | master | worker | etcd |
|-----|-----|-----|-----|-----|
| node 1 | 142.93.224.73 | • | • | • |
| node 2 | 178.62.241.216 | • | • | • |
| node 3 | 64.225.74.130 | | • | • |
| node 4 | 64.225.70.87 | | • | |
| node 5 | 178.62.243.156 | | • | |

## Said

| node | address | master | worker | etcd |
|-----|-----|-----|-----|-----|
| node 1 | 64.225.74.2 | • | • | • |
| node 2 | 157.245.71.175 | • | • | • |
| node 3 | 188.166.97.144 | | • | • |
| node 4 | 134.209.203.103 | | • | |
| node 5 | 64.225.72.118 | | • | |

