# 19a Kubernetes demo in staging

## Solution

* `kube/demo-staging.yaml`

```yaml
# demo-db
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: staging
  name: demo-db-mongodb
  labels:
    app: demo-db-mongodb
spec:
  replicas: 1
  selector:
    matchLabels:
      app: demo-db-mongodb
  template:
    metadata:
      namespace: staging
      labels:
        app: demo-db-mongodb
    spec:
      containers:
        - name: demo-db-mongodb
          image: mongo:latest
---
apiVersion: v1
kind: Service
metadata:
  namespace: staging
  labels:
    application: demo-db-mongodb
  name: demo-db-mongodb
spec:
  selector:
    app: demo-db-mongodb
  ports:
    - port: 27017
      protocol: TCP
      targetPort: 27017
---
# demo-backend
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: staging
  name: demo-backend
  labels:
    app: demo-backend
spec:
  replicas: 2
  selector:
    matchLabels:
      app: demo-backend
  template:
    metadata:
      namespace: staging
      labels:
        app: demo-backend
    spec:
      containers:
        - name: demo-backend
          image: jeankedotcom/demo-backend:v1.1
          resources:
            limits:
              cpu: 1500m
              memory: 1024Mi
            requests:
              cpu: 50m
              memory: 50Mi
---
apiVersion: v1
kind: Service
metadata:
  namespace: staging
  labels:
    application: demo-backend
  name: demo-backend
spec:
  selector:
    app: demo-backend
  ports:
    - port: 80
      protocol: TCP
      targetPort: 80
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  namespace: staging
  name: demo-backend
spec:
  rules:
    - host: demo-api-staging.goca-training.be
      http:
        paths:
          - backend:
              serviceName: demo-backend
              servicePort: 80
---
# demo-frontend
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: staging
  name: demo-frontend
  labels:
    app: demo-frontend
spec:
  replicas: 1
  selector:
    matchLabels:
      app: demo-frontend
  template:
    metadata:
      namespace: staging
      labels:
        app: demo-frontend
    spec:
      containers:
        - name: demo-frontend
          image: jeankedotcom/demo-frontend:v2.1
          env:
            - name: REST_API_URL
              value: "http://demo-api-staging.goca-training.be:31222"
          resources:
            limits:
              cpu: 1500m
              memory: 1024Mi
            requests:
              cpu: 50m
              memory: 50Mi
---
apiVersion: v1
kind: Service
metadata:
  namespace: staging
  labels:
    application: demo-frontend
  name: demo-frontend
spec:
  selector:
    app: demo-frontend
  ports:
    - port: 80
      protocol: TCP
      targetPort: 80
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  namespace: staging
  name: demo-frontend
spec:
  rules:
    - host: demo-staging.goca-training.be
      http:
        paths:
          - backend:
              serviceName: demo-frontend
              servicePort: 80
```

* add lines to `C:\Windows\System32\drivers\etc\hosts`

```shell
	127.0.0.1       demo-staging.goca-training.be
	127.0.0.1       demo-api-staging.goca-training.be
```

* deploy the demo in staging

```shell
kubectl apply -f kube/demo-staging.yaml
```

* check out <http://demo-staging.goca-training.be:31222/>
