# 07 Kubernetes dashboard

* install metrics server and dashboard

```shell
kubectl apply -f https://gitlab.com/devgem/goca-training-2020/-/raw/master/kube/metrics-server.yaml
```

* create admin user and get token

```shell
kubectl apply -f kube/admin-user.yaml
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
```

* token `eyJhbGciOiJSUzI1NiIsImtpZCI6IlU3Ymp1dVpPc3hSbzFGdVJMcnZid3NPZnlYbHNQdXZMZy1TaS1JU2NUTVkifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJhZG1pbi11c2VyLXRva2VuLWs0Y2xkIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImFkbWluLXVzZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJiYWQ0YTYxMC02NjU1LTRhMzQtYjMyNS0zYTQxM2Y5ZjE0ZmUiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06YWRtaW4tdXNlciJ9.bRG-nYytQMT8cYo8RAr69sI92aKMSvFJrmREGgwuTWynaGbXW_zpnjCR5NTjzkRYhOY-N1Fzc7ytOnsi-w5XYYgKbSkOSrIIJ8F91AlsNuwhbHv6Uj7wfbnH5racKjQCyiddGy_SF3DZ9z9MT1t_H0S2vprYynABp_001rPm1LunUMlw-W0tAAh4p2zH_hlW48ieQ77KRcIMvNR6SLH6FqgUQAPsj4mTnujiaXWj4iPhDIFYAMA1HHgKr_H23tC55MXr-6XvOsH_cXghJh46wSB-dLjdiZMJV4GweK9y3Ip7nK2mZtoms50-mPafClSNWSyPQqfQsiOHRLApQHBW4Q`

* install dashboard

```shell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0/aio/deploy/recommended.yaml
```

* wait a few seconds or minutes until all pods have started

```shell
kubectl get pods --all-namespaces
```

* start the proxy

```shell
kubectl proxy
```

* open <http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/>
* login with your token above
