# 21 Custom Helm chart

Make sure you have helm installed, it not install it:

```shell
# Windows
choco install kubernetes-helm

# Mac
brew install kubernetes-helm

# and check your helm version
helm version
```

The goal of this exersice is to create a Helm chart for the demo application.

The helm chart should be configurable so it is deployable in any namespace, can be deployed with or without Ingress and in case of Ingress the frontend and backend urls can be specified as parameters.

The chart should be based on the exercise solution in file `19a kubernetes demo in staging.md`. Helm will create a namespace automagically.

We will build the Helm chart in 6 steps:

1. create a new sample chart
2. create the `demo-db.yaml`, `demo-frontend.yaml` and `demo-bakend.yaml` files
3. make the yaml files parametrizable by putting in template variables
4. adjust the `values.yaml` file to accomodate to our needs
5. review the `Chart.yaml` file and chart folder
6. debug and test the chart locally on our local Kubernetes cluster

## 1 Create new sample chart

* create a new chart

```shell
# start from your goca-training folder
mkdir helm
cd helm
helm create demo
```

## 2 Create yaml files

* go to the `demo\templates` folder of the generated chart
* cleanup the `templates` folder:
  * delete hpa.yaml
  * delete ingress.yaml
  * delete deployment.yaml
  * delete service.yaml
  * delete serviceaccount.yaml
* create a `demo-db.yaml` based on the demo-db section of the solution in 19a
* create a `demo-frontend.yaml` based on the demo-db section of the solution in 19a
* create a `demo-backend.yaml` based on the demo-db section of the solution in 19a, make sure you have `2` replicas

## 3 Introduce parameters

Mind that whitespace handling in GO templates can be challenging (see <https://github.com/helm/helm/blob/master/docs/chart_template_guide/control_structures.md#controlling-whitespace>). The easiest configuration for conditionals is to use the following configuration (and indent the if and end statements the same as the content in between):

```yaml
{{- if CONDITION }}
...
{{- end}}
```

The following parameters should be available when using the demo Helm chart:

* if `--namespace` is given in the commandline it should be used (if no namespace is specified then the default namespace will automatically be `default`).
* `ingress.enabled` should be a "values" parameter which defaults to `true`
  * if `ingress.enabled` is `true` then ingress should be added and all services should be `ClusterIP` (this is the default for a service so you do not need to specify it)
  * if `ingress.enabled` is `false` then ingress should not be added and all services should be `NodePort`
* `service.url.frontend` should be a "values" parameter which defaults to `demo.goca-training.be`
* `service.url.backend` should be a "values" parameter which defaults to `demo-api.goca-training.be`
* `service.version.backend` should be a "values" parameter which defaults to `v1.1`

## 4 values.yaml

Make sure the following parameters are in your values.yaml in the correct format with the correct indentation and default values:

* `ingress.enabled`
* `service.url.frontend`
* `service.url.backend`
* `service.version.backend`

## 5 Chart.yaml

* give your chart `version` and `appVersion` 1.0.0
* give it a good `description`
* replace all content in `NOTES.txt` with `Application demo installing...`

## 6 Debug & Test

* if Helm and/or nginx ingress is/are not yet initialized on your local Kubernetes cluster then initialize it/them:

``` shell
# if you dit not yet install the nginx ingress then install it
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.35.0/deploy/static/provider/cloud/deploy.yaml
kubectl get services --all-namespaces
```

* dry run your cluster without installation and check the output:

```shell
# ---
# test scenario: all defaults
# ---
helm install --dry-run --debug --generate-name demo ./demo
# resulting yaml should contain
# * ingress
# * all in namespace demo
# * with no service type specification
# * with frontend url demo.goca-training.be
# * with backend url demo-api.goca-training.be
# * with backend version 1.1
# * port in REST_API_URL should be 80

# ---
# test scenario: no ingress in helmtest
# ---
helm install --dry-run --debug --generate-name --namespace helmtest --set ingress.enabled=false ./demo
# resulting yaml should contain
# * no ingress
# * in namespace helmtest
# * with backend url demo-api-helmtest.goca-training.be
# * with backend version 1.1
# * port in REST_API_URL should be 80

# ---
# deploy scenario to your local cluster with ingress in helmtest
# ---
# install the Helm chart
kubectl create namespace helmtest
helm install demo --debug --namespace helmtest --set service.url.frontend=demo-helmtest.goca-training.be --set service.url.backend=demo-api-helmtest.goca-training.be ./demo
# resulting yaml should contain
# * ingress
# * in namespace helmtest
# * with no service type specification
# * with frontend url demo-helmtest.goca-training.be
# * with backend url demo-api-helmtest.goca-training.be
# * with backend version 1.1
kubectl get pods --namespace helmtest
kubectl get services --namespace helmtest
kubectl get ingress --namespace helmtest

# set demo-helmtest.goca-training.be and demo-api-helmtest.goca-training.be in your hosts file
# and check out http://demo-helmtest.goca-training.be

# ---
# update the deployed app by setting the version of your backend to 1.2 and see the live rolling update in http://demo-helmtest.goca-training.be
# ---
helm upgrade demo --debug  --namespace helmtest --set service.url.frontend=demo-helmtest.goca-training.be --set service.url.backend=demo-api-helmtest.goca-training.be --set service.version.backend=v1.2 ./demo

# ---
# delete it all
# ---
helm ls --namespace helmtest
helm uninstall --namespace helmtest demo
helm ls --namespace helmtest
kubectl delete namespace helmtest
```
