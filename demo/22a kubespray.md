# 22a Kubespray

## Solution

* `inventory.ini`

```ini
[all]
node1 ansible_host=188.166.95.218 etcd_member_name=etcd1 ansible_ssh_private_key_file=~/kubespray/kubespray.pem
node2 ansible_host=188.166.95.224 etcd_member_name=etcd2 ansible_ssh_private_key_file=~/kubespray/kubespray.pem
node3 ansible_host=167.71.79.181 etcd_member_name=etcd3 ansible_ssh_private_key_file=~/kubespray/kubespray.pem
node4 ansible_host=167.71.68.0 ansible_ssh_private_key_file=~/kubespray/kubespray.pem
node5 ansible_host=188.166.94.44 ansible_ssh_private_key_file=~/kubespray/kubespray.pem

[kube-master]
node1
node2

[etcd]
node1
node2
node3

[kube-node]
node1
node2
node3
node4
node5

[k8s-cluster:children]
kube-master
kube-node
```
