# 10 Kubernetes ingress

## Follow along

* install the nginx ingress controller

```shell
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.35.0/deploy/static/provider/cloud/deploy.yaml
```

* get all services

```shell
kubectl get services --all-namespaces
```

* revert the my-app service back to ClusterIP

```yaml
apiVersion: v1
kind: Service
metadata:
  name: my-app-service
spec:
  selector:
    app: my-app-pod
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
```

```shell
kubectl delete -f kube/my-app-service.yaml
kubectl apply -f kube/my-app-service.yaml
```

* create a yaml file with the ingress for the my-app container in `kube/my-app-ingress.yaml`

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: my-app-ingress
spec:
  rules:
    - host: myapp.goca-training.be
      http:
        paths:
          - backend:
              serviceName: my-app-service
              servicePort: 80
```

* open `C:\Windows\System32\drivers\etc\hosts` in Notepad running as Administator
* add lines

```shell
	127.0.0.1       myapp.goca-training.be
```

* deploy the ingress

```shell
kubectl apply -f kube/my-app-ingress.yaml
```

* goto the exposed app: <http://myapp.goca-training.be>
